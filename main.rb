# url: https://www.codewars.com/kata/upside-down-numbers/ruby
# kata:
# Consider the numbers 6969 and 9116. When you rotate them 180 degrees (upside down), these numbers remain the same. To clarify, if we write them down on a paper and turn the paper upside down, the numbers will be the same. Try it and see! Some numbers such as 2 or 5 don't yield numbers when rotated.
# Given a range, return the count of upside down numbers within that range. For example, solve(0,10) = 3, because there are only 3 upside down numbers >= 0 and < 10. They are 0, 1, 8.
# More examples in the test cases.
# Good luck!
# If you like this Kata, please try Life without primes
# Please also try the performance version of this kata at Upside down numbers - Challenge Edition

def solve(a, b)
  (a..(b - 1)).map do |i|
    s = i.to_s
    if (s =~ /[23457]/).nil?
      r = s.split(//).map do |n|
        x = upsideDown(n.to_i)
        x.nil? ? '' : x.to_s
      end.reverse.join
    end
    r == i.to_s ? r : ''
  end.select {|s| s.size > 0}.count
end

def upsideDown(i)
  {0 => 0, 1 => 1, 6 => 9, 8 => 8, 9 => 6}[i]
end


puts ("3?  => #{solve(0, 10)}")
puts ("4?  => #{solve(10, 100)}")
puts ("12? => #{solve(100, 1000)}")
puts ("20? => #{solve(1000, 10000)}")
puts ("6?  => #{solve(10000, 15000)}")
puts ("9?  => #{solve(15000, 20000)}")
puts ("15?  => #{solve(60000, 70000)}")
puts ("55?  => #{solve(60000, 130000)}")
